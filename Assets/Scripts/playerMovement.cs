using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour
{

		public float speed;
		public bool isDead;
		public bool isMoving;

		public GameObject enemyController;
		public GameObject movingText;
		public GameObject feedback;
		public GameObject closest;

		public GameObject rayo;
		public GameObject dust;
		public GameObject currentDust;

		public float sizeDust;
		public GameObject bulletFeedbackObj;

	
	
		public void move ()
		{
				sizeDust = Random.Range (0.1f, 0.5f);
				if (!disparo.isLaserState) {
						#if UNITY_EDITOR 

						if (InputHelper.left ()) {
								transform.position += new Vector3 (-speed, 0, 0);
								currentDust = Instantiate (dust, transform.position, transform.rotation)as GameObject;
								currentDust.transform.Translate (new Vector3 (speed, 0, 0));
								currentDust.GetComponent<dustScr> ().offset = Random.Range (0f, 0.5f);
								currentDust.transform.localScale = new Vector3 (sizeDust, sizeDust, sizeDust);
						}
						if (InputHelper.right ()) {
								transform.position += new Vector3 (speed, 0, 0);
								currentDust = Instantiate (dust, transform.position, transform.rotation) as GameObject;
								currentDust.GetComponent<dustScr> ().offset = Random.Range (0f, 0.5f);
								currentDust.transform.localScale = new Vector3 (sizeDust, sizeDust, sizeDust);
								currentDust.transform.Translate (new Vector3 (-speed, 0, 0));
						}
						if (InputHelper.up ()) {
								transform.position += new Vector3 (0, speed, 0);
								currentDust = Instantiate (dust, transform.position, transform.rotation)as GameObject;
								currentDust.transform.Translate (new Vector3 (0, -speed, 0));
								currentDust.GetComponent<dustScr> ().offset = Random.Range (0f, 0.5f);
								currentDust.transform.localScale = new Vector3 (sizeDust, sizeDust, sizeDust);
						}
						if (InputHelper.down ()) {
								transform.position += new Vector3 (0, -speed, 0);
								currentDust = Instantiate (dust, transform.position, transform.rotation)as GameObject;
								currentDust.transform.Translate (new Vector3 (0, speed, 0));
								currentDust.GetComponent<dustScr> ().offset = Random.Range (0f, 0.5f);
								currentDust.transform.localScale = new Vector3 (sizeDust, sizeDust, sizeDust);
						}
						#endif
		
						#if UNITY_IPHONE

						Vector2 touchDeltaPosition = InputHelper.touch ();
						transform.position += new Vector3 (touchDeltaPosition.x * speed, touchDeltaPosition.y * speed, 0);
						if (speed != 0) {

								currentDust = Instantiate (dust, transform.position, transform.rotation)as GameObject;
								currentDust.transform.Translate (new Vector3 (touchDeltaPosition.x * -speed, touchDeltaPosition.y * -speed, 0));
								currentDust.GetComponent<dustScr> ().offset = Random.Range (0f, 0.5f);
								currentDust.transform.localScale = new Vector3 (sizeDust, sizeDust, sizeDust);
						}
						#endif

						cameraScript _cameraScript = Camera.main.GetComponent<cameraScript> ();

						if (transform.position.x > globales.maxX) {
								Vector3 p = new Vector3 (globales.minX, transform.position.y, 0);
								transform.position = p;
						} 

						if (transform.position.x < globales.minX) {
								Vector3 p = new Vector3 (globales.maxX, transform.position.y, 0);
								transform.position = p;

						}

						if (transform.position.y > globales.minY) {
								Vector3 p = new Vector3 (transform.position.x, globales.maxY, 0);
								transform.position = p;
						}
						if (transform.position.y < globales.maxY) {
								Vector3 p = new Vector3 (transform.position.x, globales.minY, 0);
								transform.position = p;
						}
				}

		}
		public void rotateShipDirection (enemyController _enemyController)
		{
				closest = _enemyController.getClosest (transform.position);
				if (closest) {
			
						float angle = calculateAngle (closest.transform.position);
			
						Vector3 p = new Vector3 (transform.position.x, transform.position.y, Mathf.Rad2Deg * angle);
						transform.rotation = Quaternion.Euler (p);
			
//						Debug.DrawLine (transform.position, Vector2.right, Color.white);
//						Debug.DrawLine (transform.position, Vector3.forward, Color.red);
				}
		}



		void OnTriggerEnter (Collider other)
		{

				if (other.gameObject.tag == "arana" || other.gameObject.tag == "snake") {

						GameObject go = GameObject.FindGameObjectWithTag ("GameController");
						gameControl.stageDeployed = true;
						go.GetComponent<gameControl> ().finishGame ();
						gameControl.currentState = gameControl.State.GAMEOVER;
//						go.GetComponent<gameControl> ().toInterlude ();

						Destroy (gameObject);
				}

				if (other.gameObject.tag == "paquete") {

//						print (globales.auxWeapon);
//						print (globales.currentWeapon);

//						Instantiate (feedback, transform.position, transform.rotation);
						GameObject movingTextToHUD = Instantiate (movingText, transform.position, transform.rotation)as GameObject;
						movingTextToHUD.GetComponent<movingTextScr> ().bulletType = (other.GetComponent<paqueteScr> ().bulletType);

						GameObject feedback = Instantiate (bulletFeedbackObj, transform.position, Quaternion.identity)as GameObject;

						feedback.GetComponent<BulletsFeedbackScr> ().text = ((globales.WEAPONS)other.GetComponent<paqueteScr> ().bulletType).ToString ();
//						print ("BULLET TYPE " + feedback.GetComponent<BulletsFeedbackScr> ().text);
			
				
						other.gameObject.GetComponent<paqueteScr> ().giveBullets ();
						other.gameObject.GetComponent<paqueteScr> ().triggerAnim ();
						globales.bulletsCollected++;


			
				}

				if (other.gameObject.tag == "bomba") {

						GetComponent<triggerSplashBomb> ().triggerSplash ();

						enemyController.GetComponent<enemyController> ().clearEnemies ();
//						enemyController.GetComponent<enemyController> ().createSnake ();
//						enemyController.GetComponent<enemyController> ().createSpider ();
						Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shake", 8f);
						Destroy (other.gameObject);
				}

				if (other.gameObject.tag == "rayoBomba") {
			
						GetComponent<triggerSplashBomb> ().rayobomba ();
						Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 20f);

						Destroy (other.gameObject);
				}

				if (other.gameObject.tag == "NaveProxy") {
			
						other.GetComponent<naveProxyController> ().setWorking ();

						gameObject.GetComponent<gizmosProxy> ().createGizmos ();
				}
		}
	
	
		float calculateAngle (Vector2 target)
		{
				float ay = transform.position.y;
				float ax = transform.position.x;
				float bx = target.x;
				float by = target.y;
		
				float angle = Mathf.Atan2 (by - ay, bx - ax);
				return angle;
		}
}
