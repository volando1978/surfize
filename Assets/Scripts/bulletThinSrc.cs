﻿using UnityEngine;
using System.Collections;

public class bulletThinSrc : MonoBehaviour
{

		public float speed;
		public float angle;
		public float step;
	
	
		Vector3 p;
	
		public GameObject enemyController;
		public GameObject paquete;
		public GameObject movingText;
		public GameObject movingTextKills;
		public GameObject explosion;
		public GameObject dust;

	
	
	
	

	
	
	
		public void  OnExplosion ()
		{
		
				Instantiate (explosion, transform.position, Quaternion.identity);
		
				//dust
				for (int i = 0; i< globales.dustLevel; i++) {
						Instantiate (dust, transform.position, Quaternion.identity);
				}
		
		}
	

		void OnTriggerEnter (Collider other)
		{	
				if (other.tag == "arana") {
						Destroy (gameObject);

						OnExplosion ();
						
						globales.kills += 1;
						enemyController.GetComponent<enemyController> ().deleteSpider (other.gameObject);
						//TODO: coordinar con una clase para las oleadas
			
//						enemyController.GetComponent<enemyController> ().createSpider ();
						Instantiate (paquete, transform.position, transform.rotation);
			
						Instantiate (movingTextKills, transform.position, transform.rotation);
			
						Destroy (gameObject);
				}
		
				if (other.tag == "snake") {

					
						OnExplosion ();
						
						Destroy (gameObject);
						enemyLife _enemyLife = other.GetComponent<enemyLife> ();
			
						if (_enemyLife.hp < 0) {
				
								globales.kills += 1;
								enemyController.GetComponent<enemyController> ().deleteSnake (other.gameObject);
								//TODO: coordinar con una clase para las oleadas
//								enemyController.GetComponent<enemyController> ().createSnake ();
								Instantiate (paquete, transform.position, transform.rotation);
				
								Instantiate (movingTextKills, transform.position, transform.rotation);
				
								Destroy (gameObject);
						} else {
								other.GetComponent<enemyLife> ().decreaseHP ();
						}
				}
		}
}
		
