﻿using UnityEngine;
using System.Collections;

public class disparo : MonoBehaviour
{


		public int shootTime;
		public int shotTimeNormal;
		public int shotTimewBock;
		public int shotTimeLaser;
		public int shotTimeMoire;

		public int shotTimeTWay;
		public int shotTimeTri;
		public int shotTimeCircular;


//		public int fireCounter;
		public int largo;
		public int tLaser;
		public static bool isLaserState;

//		public int speed;
		public float shotForce;

		public void setWeaponOrder ()
		{
				int values = (int)globales.unlockedWeapons;//(int[])globales.WEAPONS.GetValues (typeof(globales.WEAPONS));
//				print ("LOG DE VALORES: " + values);
				for (int i=values; i>= 0; i--) {
						if (globales.bullets [i] > 0) {

								globales.currentWeapon = (globales.WEAPONS)i;
								print ("NOW BULLETS ARE: " + globales.currentWeapon);
						}
				}


				

		}

		public void dispara (enemyController _enemyController, GameObject bullet, GameObject bulletThin, GameObject cirController, GameObject bulletLaser, GameObject raycastObj)
		{
				GameObject closest = _enemyController.getClosest (transform.position);



//				if (Input.GetKey ("space")) {

				for (int input = 0; input < Input.touchCount; ++input) {
						Time.timeScale = 1;
						setWeaponOrder ();
						if (closest) {
								if (shootTime > 1) {
			
										shootTime -= 1;
						
			
								} else if (isBullets ()) {
										switch (globales.currentWeapon) { 
				
										case globales.WEAPONS.WBLOCK:
				
												GameObject bulletw = Instantiate (bullet, transform.position, transform.rotation) as GameObject;
												bulletw.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce);

												shootTime = shotTimewBock;
												globales.bullets [(int)globales.currentWeapon] -= 1;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;
				
										case globales.WEAPONS.LASER:
												GameObject bulletL = Instantiate (bulletLaser, transform.position, transform.rotation) as GameObject;
												bulletL.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce / 80);
												bulletL.GetComponent<LineRenderer> ().SetWidth (0.2f, 0.2f);

												isLaserState = true;
												GameObject raycastL = Instantiate (raycastObj, transform.position, transform.rotation) as GameObject;
												raycastL.transform.parent = transform;
												raycastL.GetComponent<RaycastScr> ().length = 10f;

												globales.bullets [(int)globales.currentWeapon] -= 1;
												shootTime = shotTimeLaser;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;

										case globales.WEAPONS.MOIRE:

												GameObject bulletM = Instantiate (bulletLaser, transform.position, transform.rotation) as GameObject;
												bulletM.GetComponent<LineRenderer> ().SetWidth (0.01f, 0.01f);
												bulletM.GetComponent<LaserControl> ().timer = 400;

												GameObject raycastO = Instantiate (raycastObj, transform.position, transform.rotation) as GameObject;
												raycastO.GetComponent<RaycastScr> ().timer = 400;
												raycastO.GetComponent<RaycastScr> ().length = 2f;
												raycastO.transform.parent = transform;

												globales.bullets [(int)globales.currentWeapon] -= 1;
												shootTime = shotTimeMoire;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;

										case globales.WEAPONS.TWAY:
												for (int i=0; i< 3; i++) {

														Quaternion rotation = transform.rotation;//Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z);
														rotation *= Quaternion.Euler (0, 0, -30);

														rotation *= Quaternion.Euler (0, 0, i * 30);
					
														GameObject bullet1 = Instantiate (bullet, transform.position, rotation) as GameObject;
														bullet1.rigidbody.AddForce (bullet1.transform.right * shotForce); 
												}

												globales.bullets [(int)globales.currentWeapon] -= 1;
												shootTime = shotTimeTWay;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;
										case globales.WEAPONS.CIRCULAR:

												GameObject bulletCircle = Instantiate (cirController, transform.position, transform.rotation) as GameObject;
												bulletCircle.transform.parent = transform;


												globales.bullets [(int)globales.currentWeapon] -= 1;
												shootTime = shotTimeTWay;
				
												break;
										case globales.WEAPONS.BOMBA:
				
												break;
										case globales.WEAPONS.RAYO:
				
												break;
										case globales.WEAPONS.TRI:
												break;
				
										}
								} else {
										//set laser and can move
										if (isLaserState) {
												isLaserState = false;
										}

										GameObject bulletn = Instantiate (bulletThin, transform.position, transform.rotation) as GameObject;
										bulletn.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce);
										shootTime = shotTimeNormal;

								}
						}
				}


//				}
		}

		bool isBullets ()
		{
				bool isBullets = false;
//				print ("bullets? " + globales.bullets);
//				print ("bullets? " + (int)globales.currentWeapon);
//
//				print ("bullets? " + globales.bullets [(int)globales.currentWeapon]);
				if (globales.bullets [(int)globales.unlockedWeapons] != null && globales.bullets [(int)globales.currentWeapon] != 0) {
						isBullets = true;
				}

				return isBullets;
		}

}
