﻿using UnityEngine;
using System.Collections;

public class buttonTriggered : MonoBehaviour
{

		public GUIStyle buttonSt = new GUIStyle ();

		public int stateTo ;
		public GameObject gameControlObj;
		public Rect posH;

		public LayerMask UI;
		Ray ray;
		RaycastHit hit;


		// Use this for initialization
		void Start ()
		{
				if (gameObject.tag != "unit") {

						GetComponent<Animator> ().speed = 0;
				}		
		}
	
		// Update is called once per frame
		void Update ()
		{

				#if UNITY_IOS
				if (Input.touchCount > 0 && 
						Input.GetTouch (0).phase == TouchPhase.Began) {
						ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, UI) && hit.collider == gameObject.collider) {
								if (gameObject.tag != "unit") {
										GetComponent<Animator> ().speed = 1;
								}
						}

				}
		
		
				
				#endif
		
				#if UNITY_EDITOR
		
				if (InputHelper.mouseClick ()) {
//				if (Input.GetMouseButtonDown (0)) {
////						print ("pressed: ");
//
//						Vector2 p = (Input.mousePosition);
//						Ray ray = Camera.main.ScreenPointToRay (p);
//			
//						RaycastHit hit;
//			
//
//						//toca item
//						if (Physics.Raycast (ray, out hit, Mathf.Infinity) && hit.collider.tag == "unit") { // layerMask
//
//								//startStage
////								gameControl.Instance.loadStage (hit.collider.gameObject.GetComponent<number> ().n);
//					
//					
////								print ("w: " + hit.collider.tag);
//						}

			
				}
				#endif
		
		
		}

		public void triggerButton ()
		{
//				if (Input.touchCount > 0 && 
//						Input.GetTouch (0).phase == TouchPhase.Began) {
//						ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
////						RaycastHit hit;
//						print ("aqui");
//						if (Physics.Raycast (ray, out hit, Mathf.Infinity, UI) && hit.collider == gameObject.collider) {
				if (gameObject.tag != "unit") {

						switch (stateTo) {
						case 0:
								gameControlObj = GameObject.FindGameObjectWithTag ("GameController");
								gameControlObj.GetComponent<gameControl> ().initMenu ();
					
								break;
					
						case 3:
								gameControlObj = GameObject.FindGameObjectWithTag ("GameController");
					
								gameControlObj.GetComponent<gameControl> ().toMap ();
								break;
						}
				
						gameControl.currentState = (gameControl.State)stateTo;
						Destroy (gameObject.transform.parent.gameObject);
				} else {

						print ("TAG " + gameObject.tag + " layer " + hit.collider.gameObject.layer);	
						GameObject avatar = GameObject.FindGameObjectWithTag ("avatar");
						print ("vurrent s?" + globales.currentStage);
				
						//set current stage to move
						//										if (gameControl.maps [globales.currentMap].stages [globales.currentStage].isStageCleared)
						globales.currentStage = gameObject.GetComponent<number> ().n;
						print ("vurrent s?" + globales.currentStage);
				
						//operates, move ship
						MapScr parentMap = gameObject.GetComponentInParent<MapScr> ();
				
						if (avatar.transform.position != (Vector3)parentMap.positions [globales.currentStage]) {
								avatar.transform.position = Vector3.MoveTowards (avatar.transform.position, (Vector3)parentMap.positions [globales.currentStage], parentMap.speedTransition * Time.deltaTime);
								print ("moving");						
						}
						parentMap.StartCoroutine ("waiting");
				}
			
			
//						}
//				} 
		}

		void OnGUI ()
		{
				Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
				pos.y = Screen.height - pos.y - 40f;
		
				if (gameObject.tag != "unit") {
						switch ((int)(gameControl.State)stateTo) {
						case 0:
								GUI.Label (new Rect (pos.x - 148f, pos.y, 100, 100), "MENU", buttonSt);
								break;
						case 3:
								GUI.Label (new Rect (pos.x - 148f, pos.y, 100, 100), "RETRY", buttonSt);
								break;
						}
				}


		}
}
