﻿using UnityEngine;
using System.Collections;

public class LaserControl : MonoBehaviour
{
		public bool isShowingLaser = false;
		Color yellow = Color.yellow;
		public int timer = 60;




		// Update is called once per frame
		void Update ()
		{

				timer--;
				if (timer < 0) {
						Destroy (gameObject);
				}

				transform.position = new Vector3 (transform.position.x, transform.position.y, 0);

		}



		IEnumerator  showLaser ()
		{

				isShowingLaser = true;
				GetComponent<LineRenderer> ().SetColors (yellow, yellow);
				this.renderer.enabled = true;
				GetComponent<LineRenderer> ().SetColors (yellow, yellow);

				yield return new WaitForSeconds (.05f);
				resetLaser ();
				isShowingLaser = false;
		}

		public void resetLaser ()
		{    
				this.renderer.enabled = false;
				GetComponent<LineRenderer> ().SetColors (yellow, yellow);

		}
	


}
