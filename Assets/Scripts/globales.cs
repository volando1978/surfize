﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;


public class globales : MonoBehaviour
{

		[SerializeField]
		int
				frameRate = 60;

		public static int[] bullets = new int[WEAPONS.GetValues (typeof(WEAPONS)).Length];

		public static int kills;
		public static int maxKills;
		public static int lastKills;
		public static int bulletsCollected;

		public static int hview;
		public static int wview;

		public static float minX ;
		public static float maxX ;
		public static float minY ;
		public static float maxY ;

	
		public static WEAPONS
				currentWeapon;

		public static WEAPONS
				auxWeapon;

		public static WEAPONS
				unlockedWeapons;

		//settings
		public static int
				dustLevel;

		public static int
				currentMap;

		public static int
				totalNumberMaps; 
		public static int
				currentStage;

		public static int clearedStagesNumber;

		public enum WEAPONS
		{
				WBLOCK=0,
				LASER=1,
				TWAY=2,
				CIRCULAR=3,
				TRI=4,
				BOMBA=5,
				RAYO=6,
				MOIRE = 7
		}
	
		public void Awake ()
		{
				Application.targetFrameRate = frameRate;
				getData ();

		}


	
		// Use this for initialization
		void Start ()
		{

				setRandomColor ();
				//camera bounds
				float camHeight = Camera.main.orthographicSize * 1.8f;   
				float camWidth = camHeight * Camera.main.aspect;
		
				// Calculations assume map is position at the origin
				globales.minX = 0 - camWidth / 2;
				globales.maxX = 0 + camWidth / 2;
				globales.minY = 1f + camHeight / 2;
				globales.maxY = 0 - camHeight / 2;

				dustLevel = 5;


		}
	
		// Update is called once per frame
		void Update ()
		{
				#if UNITY_EDITOR 

				if (Input.GetKeyDown (KeyCode.A)) {
//						bool next = false;
//						foreach (WEAPONS w in WEAPONS.GetValues(typeof(WEAPONS))) {
//								print ("ARMA: " + w);
//
//								if (next == true) {
//										globales.currentWeapon = w;
//										break;
//								}
//
//								if (globales.currentWeapon == w) {
//										next = true;
//								}			
//						}

						deleteData ();

				}

				#endif
				#if UNITY_IPHONE

				if (Input.touchCount > 1 && Input.GetTouch (0).phase == TouchPhase.Began) {
//						bool next = false;
//						foreach (WEAPONS w in WEAPONS.GetValues(typeof(WEAPONS))) {
//								print ("ARMA: " + w);
//				
//								if (next == true) {
//										globales.currentWeapon = w;
//										break;
//								}
//				
//								if (globales.currentWeapon == w) {
//										next = true;
//								}
//						}
						deleteData ();
				}

				#endif
		}

//		public static void resetStats ()
//		{
//				bullets = 0;
//				kills = 0;
//				maxKills = 0;
//				currentWeapon = globales.WEAPONS.WBLOCK;
//
//
//		}

	
		public static void clearMenu ()
		{
				GameObject[] gos = GameObject.FindGameObjectsWithTag ("menu");
				foreach (GameObject go in gos) {
						Destroy (go);
				}

//				GameObject[] ene = GameObject.FindGameObjectsWithTag ("snake");
//				foreach (GameObject e in ene) {
//						Destroy (e);
//				}
//				GameObject[] spi = GameObject.FindGameObjectsWithTag ("arana");
//				foreach (GameObject s in spi) {
//						Destroy (s);
//				}
		}

		public static int getWeaponType ()
		{

//				int[] values = WEAPONS.GetValues (typeof(WEAPONS));
				int value;// = (int)globales.unlockedWeapons;
//				print ("UNLOCKED WEAPON: " + (int)globales.unlockedWeapons);
				value = Random.Range (0, (int)globales.unlockedWeapons + 1);
//				for (int i=0; i < values.Length; i++) {
//						
//				}
//				print ("UNLOCKED: " + (int)globales.unlockedWeapons);
//				print ("RETURN VALUE: " + value);
				return value;
		}

		public static Vector2 getRandomPos ()
		{

				float rx = Random.Range (0.1f, 0.9f);
				float ry = Random.Range (0.1f, 0.9f);

				Vector2 rPos = (Vector2)Camera.main.ViewportToWorldPoint (new Vector3 (rx, ry, 0));
				return rPos;

		}

		public static Vector3 getRandomRot ()
		{
		
				float rx = Random.Range (0.1f, 0.9f);
				float ry = Random.Range (0.1f, 0.9f);
				float rz = Random.Range (0f, 360f);
		
				Vector3 rRot = new Vector3 (rx, ry, rz);

				return rRot;
		
		}

		public static void setRandomColor ()
		{
				Color32 r;

				byte n = (byte)Random.Range (80, 128);
				r.r = (byte)Random.Range (10, 128);
				r.g = (byte)Random.Range (10, 128);
				r.b = (byte)Random.Range (10, 128);
				r.a = (byte)Random.Range (10, 128);
		
				Camera.main.backgroundColor = r;
		}

		public static void setGrey ()
		{

				Color32 r;
		
				byte n = (byte)85;
				r.r = n;
				r.g = n;
				r.b = n;
				r.a = n;
		
				Camera.main.backgroundColor = r;


		}

		public static void lanzaEnDirecciones (GameObject go)
		{
				ArrayList abec = new ArrayList ();
				float len = 10f;

				if (go) {

						for (int i = 0; i<=360; i+=120) {
			
								GameObject option = (GameObject)Instantiate (go);
								abec.Add (option);
								float xD = Mathf.Cos ((float)i * Mathf.Deg2Rad);
								float yD = Mathf.Sin ((float)i * Mathf.Deg2Rad);
			
								Vector3 pos = new Vector3 (option.transform.position.x + len * xD, option.transform.position.y + len * yD, 0);
								option.transform.position = Vector3.Lerp (option.transform.position, pos, Time.deltaTime * 100f);
								
//						option.transform.parent = transform;
						}
				}


		}


	
	
	
		//--PLAYERPREFS

		public static void setData ()
		{

				PlayerPrefs.SetInt ("savedCurrentMap", currentMap);
				PlayerPrefs.SetInt ("savedCurrentStage", currentStage);
				PlayerPrefs.SetInt ("savedKills", kills);
				PlayerPrefs.SetInt ("savedMaxKills", maxKills);
				if (bullets != null) {
//						print ("existe esto? " + bullets);
						for (int i= 0; i < bullets.Length; i++) {
//								print ("hay algo dentro de  esto? " + bullets);

								if (bullets [i] != null) {
//										print ("salva esto? " + bullets);

										PlayerPrefs.SetInt ("savedBullets" + i.ToString (), bullets [i]);
//										print ("savedBullets" + i.ToString () + "  valor " + bullets [i]);
								}
						}
				}
//				PlayerPrefs.SetInt ("savedBullets0", bullets [0]);
//				PlayerPrefs.SetInt ("savedBullets1", bullets [1]);
//				PlayerPrefs.SetInt ("savedBullets2", bullets [2]);
//				PlayerPrefs.SetInt ("savedBullets3", bullets [3]);
//				PlayerPrefs.SetInt ("savedBullets4", bullets [4]);
//				PlayerPrefs.SetInt ("savedBullets5", bullets [5]);
//				PlayerPrefs.SetInt ("savedBullets6", bullets [6]);
//				PlayerPrefs.SetInt ("savedBullets7", bullets [7]);
				PlayerPrefs.SetInt ("savedUnlockedWeapons", (int)globales.unlockedWeapons);

		
				PlayerPrefs.Save ();

//				print ("graba mapa: " + PlayerPrefs.GetInt ("savedCurrentMap"));
//				print ("graba stage: " + PlayerPrefs.GetInt ("savedCurrentStage"));
//				print ("graba kills: " + PlayerPrefs.GetInt ("savedKills"));
//				print ("graba maxKills: " + PlayerPrefs.GetInt ("savedMaxKills"));
//				print ("graba maxBullets: " + PlayerPrefs.GetInt ("savedBullets"));
//				print ("graba UnlockedWeapons: " + PlayerPrefs.GetInt ("savedUnlockedWeapons"));
				print ("SAVE");

		} 

		void getData ()
		{
				print ("GETTING DATA");
				if (PlayerPrefs.HasKey ("savedCurrentMap")) {
						currentMap = PlayerPrefs.GetInt ("savedCurrentMap");
				} else {
						deleteData ();
						return;
				}

				if (PlayerPrefs.HasKey ("savedCurrentStage")) {
						currentStage = PlayerPrefs.GetInt ("savedCurrentStage");
				} else {
						deleteData ();
						return;
				} 

				if (PlayerPrefs.HasKey ("savedKills")) {
						kills = PlayerPrefs.GetInt ("savedKills");
				} else {
						deleteData ();
						return;
				}

				if (PlayerPrefs.HasKey ("savedMaxKills")) {
						maxKills = PlayerPrefs.GetInt ("savedMaxKills");
				} else {
						deleteData ();
						return;
				} 


				if (bullets != null) {
//						print ("existe esto? " + bullets);
						for (int i= 0; i < bullets.Length; i++) {
//								print ("hay algo dentro de  esto? " + bullets);
				
								if (bullets [i] != null) {
//										print ("salva esto? " + bullets);
										if (PlayerPrefs.HasKey ("savedBullets" + i.ToString ())) {
												bullets [i] = PlayerPrefs.GetInt ("savedBullets" + i.ToString ());
//												print ("savedBullets" + i.ToString () + "  valor  " + bullets [i]);
										} else {
												deleteData ();
												return;
										} 
//					PlayerPrefs.SetInt ("savedBullets" + i.ToString (), bullets [i]);
					
								}
						}
				}


//				if (PlayerPrefs.HasKey ("savedBullets0")) {
//						bullets [0] = PlayerPrefs.GetInt ("savedBullets0");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets1")) {
//						bullets [1] = PlayerPrefs.GetInt ("savedBullets1");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets2")) {
//						bullets [2] = PlayerPrefs.GetInt ("savedBullets2");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets3")) {
//						bullets [3] = PlayerPrefs.GetInt ("savedBullets3");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets4")) {
//						bullets [4] = PlayerPrefs.GetInt ("savedBullets4");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets5")) {
//						bullets [5] = PlayerPrefs.GetInt ("savedBullets5");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets6")) {
//						bullets [6] = PlayerPrefs.GetInt ("savedBullets6");
//				} else {
//						deleteData ();
//						return;
//				} 
//				if (PlayerPrefs.HasKey ("savedBullets7")) {
//						bullets [7] = PlayerPrefs.GetInt ("savedBullets7");
//				} else {
//						deleteData ();
//						return;
//				} 

				if (PlayerPrefs.HasKey ("savedUnlockedWeapons")) {
						int w = PlayerPrefs.GetInt ("savedUnlockedWeapons");
						unlockedWeapons = (globales.WEAPONS)w;
//						print ("auqi" + PlayerPrefs.GetInt ("savedUnlockedWeapons"));
				} else {
						deleteData ();
						return;
				} 

				PlayerPrefs.Save ();

		
//				print ("--carga mapa: " + PlayerPrefs.GetInt ("savedCurrentMap"));
//				print ("--carga stage: " + PlayerPrefs.GetInt ("savedCurrentStage"));
//				print ("--carga kills: " + PlayerPrefs.GetInt ("savedKills"));
//				print ("--carga maxKills: " + PlayerPrefs.GetInt ("savedMaxKills"));
//				print ("--carga bullets: " + PlayerPrefs.GetInt ("savedBullets"));
				print ("LOAD");

		}

		void deleteData ()
		{
				PlayerPrefs.DeleteAll ();
				currentMap = 0;
				currentStage = 0;
				kills = 0;
				maxKills = 0;
//				bullets [0] = 0;	
//				bullets [1] = 0;
//				bullets [2] = 0;
//				bullets [3] = 0;
//				bullets [4] = 0;
//				bullets [5] = 0;
//				bullets [6] = 0;
//				bullets [7] = 0;
				unlockedWeapons = 0;
				GameObject map = GameObject.FindGameObjectWithTag ("mapRoom");
				if (map) {
						Destroy (map);
				}
				PlayerPrefs.Save ();

				print ("no data");
		}

		public void OnApplicationQuit ()
		{
//				Save ();
				setData ();
				PlayerPrefs.Save ();
//				Debug.Log ("--APP QUIT carga mapa: " + PlayerPrefs.GetInt ("savedCurrentMap"));
//				Debug.Log ("--APP QUIT carga stage: " + PlayerPrefs.GetInt ("savedCurrentStage"));
//				Debug.Log ("--APP QUIT carga kills: " + PlayerPrefs.GetInt ("savedKills"));
//				Debug.Log ("--APP QUIT carga maxKills: " + PlayerPrefs.GetInt ("savedMaxKills"));
//				Debug.Log ("--APP QUIT carga bullets: " + PlayerPrefs.GetInt ("savedBullets"));
				Destroy (gameObject);
		}

	
		//:::::::::::::::SAVELOAD:::::::::::::
	
	
//		public  string GetiPhoneDocumentsPath ()
//		{ 
//				// Your game has read+write access to /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/Documents 
//				// Application.dataPath returns              
//				// /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/myappname.app/Data 
//				// Strip "/Data" from path 
//				string path = Application.dataPath.Substring (0, Application.dataPath.Length - 5); 
//				// Strip application name 
//				path = path.Substring (0, path.LastIndexOf ('/'));  
//				print ("SAVING PATH1" + path);
//				Debug.Log ("SAVING PAT: " + path);
//				return path + "/Documents"; 
//		}
	
		public  void Save ()
		{
//				if (Application.platform == RuntimePlatform.IPhonePlayer) {
//						savePath = GetiPhoneDocumentsPath ();
//				} else { 
//						savePath = Application.persistentDataPath;
//				} 
//		
//				//				Game.updateGame ();
//				BinaryFormatter bf = new BinaryFormatter ();
//				FileStream file = File.Create (savePath + "/savedGames.gd");
//				Debug.Log ("AQUI: PATH: " + savePath);
//				bf.Serialize (file, globales.current);
//				file.Close ();
//		
//				Debug.Log ("-------SAVING----------");
//				Debug.Log ("--SAVING map " + globales.current.currentMap);
//				Debug.Log ("--SAVING stage " + globales.current.currentStage);
//				Debug.Log ("--SAVING kills " + globales.current.currentkills);
//				Debug.Log ("--SAVING maxKills " + globales.current.currentMaxKills);
//				Debug.Log ("--SAVING Bullets " + globales.current.currentBullets);
//				Debug.Log ("-----------------");
		
		}
	
		public  void Load ()
		{
//		
//				if (Application.platform == RuntimePlatform.IPhonePlayer) {
//						savePath = GetiPhoneDocumentsPath ();
//				} else { 
//						savePath = Application.persistentDataPath;
//				} 
//		
//		
//				if (File.Exists (Application.persistentDataPath + "/savedGames.gd")) {
//						BinaryFormatter bf = new BinaryFormatter ();
//						FileStream file = File.Open (savePath + "/savedGames.gd", FileMode.Open);
//						Debug.Log ("AQUI: PATH: " + savePath);
//			
//						globales.current = (Game)bf.Deserialize (file);
//			
//						file.Close ();
//						Debug.Log ("--LOADING map " + globales.current.currentMap);
//						Debug.Log ("--LOADING stage " + globales.current.currentStage);
//						Debug.Log ("--LOADING kills " + globales.current.currentkills);
//						Debug.Log ("--LOADING maxKills " + globales.current.currentMaxKills);
//						Debug.Log ("--LOADING Bullets " + globales.current.currentBullets);
//			
//						globales.currentMap = globales.current.currentMap;
//						globales.currentStage = globales.current.currentStage;
//						globales.kills = globales.current.currentkills;
//						globales.maxKills = globales.current.currentMaxKills;
//						globales.bullets = globales.current.currentBullets;
//			
//						Debug.Log ("--LOADING 2 map " + globales.current.currentMap);
//						Debug.Log ("--LOADING 2 stage " + globales.current.currentStage);
//						Debug.Log ("--LOADING 2 kills " + globales.current.currentkills);
//						Debug.Log ("--LOADING 2 maxKills " + globales.current.currentMaxKills);
//						Debug.Log ("--LOADING 2 Bullets " + globales.current.currentBullets);
		}
//		}
	
}
