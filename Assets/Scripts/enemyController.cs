﻿using UnityEngine;
using System.Collections;

public class enemyController : MonoBehaviour
{
		public GameObject hole;
		public GameObject snake;
		public GameObject spider;
		public GameObject explosion;

		public static ArrayList posHoles = new ArrayList ();
		public static ArrayList  enemies = new ArrayList ();


//		public void initHoles ()
//		{
//
//				GameObject[] gos = GameObject.FindGameObjectsWithTag ("hole");
//
//				foreach (GameObject go in gos) {
//						print ("holeP: " + go.transform.position);
//						Vector2 holePos = new Vector2 (go.transform.position.x, go.transform.position.y);
//						posHoles.Add (holePos);
//				}
//		
//		}
	
		public void moveEnemies ()
		{
				foreach (GameObject go in enemies) {
						go.GetComponent<enemyMov> ().move ();
				}

		}

		public void createHoles ()
		{

				Vector3 v = globales.getRandomPos ();
				v.z = 0.01f;
				GameObject sp = (GameObject)Instantiate (hole, v, Quaternion.identity);
//				if (!isColliding (sp)) {
				posHoles.Add (sp);
//				}
//				print ("creatHole: " + posHoles);

		}

//		public bool isColliding (Vector2 v)
//		{
//				bool valor = false;
//				GameObject[] gos = GameObject.FindGameObjectsWithTag ("hole");
//				foreach (GameObject go in gos) {
//						if (v  (Vector2)go.transform.position) {
//								valor = true;
//						}
//						print ("VALOR COLIDING " + valor);
//				}
//				return valor;
//		}
		public void deleteHoles ()
		{
				GameObject[] gos = GameObject.FindGameObjectsWithTag ("hole");

				foreach (GameObject go in gos) {
			
						Destroy (go);
				}
				posHoles.Clear ();
//				print ("count DELETED " + posHoles.Count);

		}

		public void removeHole (GameObject go)
		{

				posHoles.Remove (go);

		}


		public void createSpider ()
		{
				GameObject newSpider = (GameObject)posHoles [Random.Range (0, posHoles.Count)];
				Vector2 v = newSpider.transform.position;//(Vector2)posHoles [Random.Range (0, posHoles.Count)];
				GameObject sp = (GameObject)Instantiate (spider, v, Quaternion.identity);
				enemies.Add (sp);

		}

		public void deleteSpider (GameObject sp)
		{
				if (enemies.Contains (sp)) {
						enemies.Remove (sp);
						Destroy (sp);
				}

		}

		public void createSnake ()
		{
//				print ("count " + posHoles.Count);
				GameObject newSnake = (GameObject)posHoles [Random.Range (0, posHoles.Count)];
				Vector2 v = newSnake.transform.position;//(Vector2)posHoles [Random.Range (0, posHoles.Count)];
				GameObject sp = Instantiate (snake, v, Quaternion.identity)as GameObject;
				enemies.Add (sp);
		}

		public void deleteSnake (GameObject sn)
		{
				sn.GetComponent<headScr> ().destroyParts ();
				if (enemies.Contains (sn)) {
						enemies.Remove (sn);
						//						print ("spider delete array");
						Destroy (sn);
				}
		
		}

		public void clearEnemies ()
		{
				foreach (GameObject go in enemies) {
						Instantiate (explosion, go.transform.position, Quaternion.identity);

						if (go.tag == "snake") {
								go.GetComponent<headScr> ().destroyParts ();
						}
						Destroy (go);
				}

				enemies.Clear ();
		}

		public  int getNumberEnemies ()
		{
//				print (enemyController.enemies.Count);
				return enemies.Count;
		}


		public GameObject getClosest (Vector3 v)
		{

				float minDist = 1000000f;
				GameObject closest = null;
				foreach (GameObject enemy in enemies) {
						if (enemy) {

								Vector3 oPos = enemy.transform.position;
								float dist = Vector3.Distance (v, oPos);

								if (dist < minDist) {
										minDist = dist;
										closest = enemy;
								}
						}
				}

				return closest;
		}
}
