using UnityEngine;
using System.Collections.Generic;

public class weaponRoom : MonoBehaviour
{

		public GameObject controller;

		public GameObject frame;
		public GameObject currentFrame;
		public GUIStyle marqueeSt = new GUIStyle ();
		public List<string> marqueText = new List<string> ();
		public float scrollSpeed = 50;
		Rect messageRect;
	
		public GameObject sprWeapon1;
		public GameObject sprWeapon2;
		public GameObject sprWeapon3;
		public GameObject sprWeapon4;
		public GameObject sprWeapon5;
	
	
		public float offsetWeaponIcon;

		public int marginLeft;
		public int marginTop;


		List<GameObject> frames = new List<GameObject> ();
		public Sprite idle  ;
	
		public LayerMask weaponMask;

		public GUIStyle boxStyle = new GUIStyle ();
		Rect boxRect;
		public float offsetForStripe;


	
		// Use this for initialization
		void Start ()
		{

				boxRect = new Rect (0, Screen.height / offsetForStripe, 0, 0);

				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO WHITE BLOCK WHITE BLOCK WHITE BLOCK WHITE BLOCK WHITE BLOCK WHITE BLOCK ");
				marqueText.Add (" LASER  LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER LASER ");
				marqueText.Add (" 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY 3 WAY ");
				marqueText.Add (" CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE ");
				marqueText.Add (" MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE MOIRE  ");
				marqueText.Add (" RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS RAYS ");
				marqueText.Add ("SDF");
				marqueText.Add ("SDF");

				drawWeaponFrames ();

		}

		public  void drawWeaponFrames ()
		{
				int indexWeapon = (int)globales.unlockedWeapons;

//				print ("unlockedWeapons: " + (int)globales.unlockedWeapons);
//				print ("indexWeapon: " + indexWeapon);
		
				for (int i= 0; i <= indexWeapon; i++) {
//						for (int j= 0; j <= indexWeapon; j++) {

						Vector2 pos = Vector2.zero;

						pos = new Vector2 (marginLeft + (frame.GetComponent<SpriteRenderer> ().bounds.size.x * (i % 5)), (marginTop + frame.GetComponent<SpriteRenderer> ().bounds.size.y * -(i / 5)));// -2.5f
//						print ("divis: " + i % 5 + "  " + i / 5);



						showWeapon (i, pos);
						GameObject f = Instantiate (currentFrame, pos, Quaternion.identity) as GameObject;
						f.transform.parent = transform;
						frames.Add (f);
			
						if ((int)globales.currentWeapon != i) {
								f.GetComponent<Animator> ().speed = 0;
						}
				}
		}
		

		public void showWeapon (int index, Vector2 pos)
		{
				//				print ("hey");
				GameObject _sp;
				Vector2 offsetPos;
				switch (index) {
			
				case 0:
						_sp = Instantiate (sprWeapon1) as GameObject;//gameObject.AddComponent<SpriteRenderer> ();
						_sp.transform.parent = transform;
						offsetPos = new Vector2 (pos.x + offsetWeaponIcon, pos.y - offsetWeaponIcon);
						_sp.transform.position = offsetPos;
			
						break;
			
				case 1:
						_sp = Instantiate (sprWeapon2) as GameObject;//gameObject.AddComponent<SpriteRenderer> ();
						_sp.transform.parent = transform;
						offsetPos = new Vector2 (pos.x + offsetWeaponIcon, pos.y - offsetWeaponIcon);
						_sp.transform.position = offsetPos;
			
						break;
			
				case 2:
						_sp = Instantiate (sprWeapon3) as GameObject;//gameObject.AddComponent<SpriteRenderer> ();
						_sp.transform.parent = transform;
						offsetPos = new Vector2 (pos.x + offsetWeaponIcon, pos.y - offsetWeaponIcon);
						_sp.transform.position = offsetPos;
			
						break;
				case 3:
						_sp = Instantiate (sprWeapon4) as GameObject;//gameObject.AddComponent<SpriteRenderer> ();
						_sp.transform.parent = transform;
						offsetPos = new Vector2 (pos.x + offsetWeaponIcon, pos.y - offsetWeaponIcon);
						_sp.transform.position = offsetPos;
			
						break;
				case 4:
						_sp = Instantiate (sprWeapon5) as GameObject;//gameObject.AddComponent<SpriteRenderer> ();
						_sp.transform.parent = transform;
						offsetPos = new Vector2 (pos.x + offsetWeaponIcon, pos.y - offsetWeaponIcon);
						_sp.transform.position = offsetPos;
			
						break;
			
				}
		
		}
	
		// Update is called once per frame
		void Update ()
		{
				#if UNITY_IOS
				if (Input.touchCount > 0 && 
						Input.GetTouch (0).phase == TouchPhase.Began) {
						Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			
						RaycastHit hit;
			
//						print ("ray IOS");
//						print ("wp: " + (int)weaponMask);
			
						//toca item
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon1") {
								globales.currentWeapon = globales.WEAPONS.WBLOCK;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon2") {
								globales.currentWeapon = globales.WEAPONS.LASER;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon3") {
								globales.currentWeapon = globales.WEAPONS.TWAY;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon4") {
								globales.currentWeapon = globales.WEAPONS.CIRCULAR;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
			
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon5") {
								globales.currentWeapon = globales.WEAPONS.MOIRE;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
				}
				#endif
		
				#if UNITY_EDITOR

				if (InputHelper.leftDown ()) {
			
						int numberL = Mathf.Max (0, (int)(globales.currentWeapon - 1));
						globales.currentWeapon = (globales.WEAPONS)numberL;
						updateFramesSelection ();
//						print ("w: " + (int)globales.currentWeapon);
			
				}
		
				if (InputHelper.rightDown ()) {
			
						int numberR = Mathf.Min ((int)(globales.currentWeapon + 1), 7);
						globales.currentWeapon = (globales.WEAPONS)numberR;
						updateFramesSelection ();
//						print ("w: " + (int)globales.currentWeapon);
			
				}
		
				if (InputHelper.mouseClick ()) {
			
						Vector2 p = (Input.mousePosition);
						Ray ray = Camera.main.ScreenPointToRay (p);
//						print ("wp: " + (int)weaponMask);
			
						RaycastHit hit;
			
//						print ("ray EDITOR");
//						print (Input.mousePosition);	
			
			
						//toca item
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon1") {
								globales.currentWeapon = globales.WEAPONS.WBLOCK;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);

						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon2") {
								globales.currentWeapon = globales.WEAPONS.LASER;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon3") {
								globales.currentWeapon = globales.WEAPONS.TWAY;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon4") {
								globales.currentWeapon = globales.WEAPONS.CIRCULAR;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
						if (Physics.Raycast (ray, out hit, Mathf.Infinity, weaponMask) && hit.collider.tag == "weaponIcon5") {
								globales.currentWeapon = globales.WEAPONS.MOIRE;
								updateFramesSelection ();
//								print ("w: " + (int)globales.currentWeapon);
				
						}
			
				}
				#endif
		}

		public void updateFramesSelection ()
		{
				int indexCurrentWeapon = (int)globales.currentWeapon;
//				print ("current weapon : " + indexCurrentWeapon);
		
				for (int i= 0; i < frames.Count; i++) {
			
						frames [i].GetComponent<Animator> ().enabled = false;
						frames [i].GetComponent<SpriteRenderer> ().sprite = idle;
			
						if (indexCurrentWeapon == i) {
								frames [i].GetComponent<Animator> ().enabled = true;
								frames [i].GetComponent<Animator> ().speed = 0.5f;
						}
				}
		}
	
		void OnGUI ()
		{

				// black box in background
				GUI.Box (boxRect, "", boxStyle);
		
				if (boxRect.height < 70f) {
						boxRect.height += 80f;
				}
		
				if (boxRect.width < Screen.width) {
						boxRect.width += 80f;
				}

				// Set up message's rect if we haven't already.
				if (messageRect.width == 0) {
						var dimensions = GUI.skin.label.CalcSize (new GUIContent (marqueText [(int)globales.currentWeapon]));
			
						// Start message past the left side of the screen.
						messageRect.x = -dimensions.x;
						messageRect.width = dimensions.x;
						messageRect.height = dimensions.y;
				}
		
				messageRect.x += Time.deltaTime * scrollSpeed;
				messageRect.y = Screen.height - messageRect.height + 5;
		
		
				// If message has moved past the right side, move it back to the left.
				if (messageRect.x > Screen.width) {
						messageRect.x = -messageRect.width;
				}
		
				GUI.Label (messageRect, marqueText [(int)globales.currentWeapon], marqueeSt);
				GUI.Label (new Rect (16f, Screen.height / offsetForStripe + Screen.height / 200f, 100, 100), "SELECT YOUR WEAPON", marqueeSt);
				GUI.Label (new Rect (16f, Screen.height / (offsetForStripe - 0.1f), 100, 100), "NEXT STAGE: " + (globales.currentStage + 1).ToString (), marqueeSt);



//				GUI.Label (new Rect (16f, Screen.height - 320f, 100, 100), "NEXT STAGE: " + (globales.currentStage + 1).ToString (), marqueeSt);
//				GUI.Label (new Rect (16f, Screen.height - 320f, 100, 100), "NEXT STAGE: " + (globales.currentStage + 1).ToString (), marqueeSt);

		}
	
		public void OnApplicationQuit ()
		{
				Destroy (gameObject);
		}
}
