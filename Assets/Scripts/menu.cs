﻿using UnityEngine;
using System.Collections.Generic;

public class menu : MonoBehaviour
{

		public List<string> marqueText = new List<string> ();
		public GameObject controller;
		public LayerMask weaponMask;
		public float scrollSpeed = 50;
		public GUIStyle marqueeSt = new GUIStyle ();
		public GUIStyle startSt = new GUIStyle ();
		Rect messageRect;
		int t = 0;

	
	
		// Use this for initialization
		public void Start ()
		{
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");
				marqueText.Add (" KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO  KILL KILL SURVIVE KILL SURVIVE PAVO ");

		}
	
		void OnGUI ()
		{
				// Set up message's rect if we haven't already.
				if (messageRect.width == 0) {
						var dimensions = GUI.skin.label.CalcSize (new GUIContent (marqueText [(int)globales.currentWeapon]));
			
						// Start message past the left side of the screen.
						messageRect.x = -dimensions.x;
						messageRect.width = dimensions.x;
						messageRect.height = dimensions.y;
						messageRect.y = Screen.height - messageRect.height - dimensions.y;
				}
		
				messageRect.x += Time.deltaTime * scrollSpeed;

		
				// If message has moved past the right side, move it back to the left.
				if (messageRect.x > Screen.width) {
						messageRect.x = -messageRect.width;
				}
		
				GUI.Label (messageRect, marqueText [(int)globales.currentWeapon], marqueeSt);

				t++;
				if (t % 100 < 50) {
						GUI.Label (new Rect (Screen.width / 2, Screen.height / 1.4f, 100, 100), "PRESS START", startSt);
				}


		
		}
	
		public void OnApplicationQuit ()
		{
				Destroy (gameObject);
		}

}
