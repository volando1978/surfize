﻿using UnityEngine;
using System.Collections;

public class RaycastScr : MonoBehaviour
{
		public int timer = 30;

		public GameObject enemyController;
		public GameObject explosion;
		public GameObject paquete;
		public GameObject movingText;
		public GameObject movingTextKills;

		public float length;
//		public GameObject explosionDelayed;
		public GameObject dust;

	
	
	
	
//		void  OnExplosionDelayed (GameObject other)
//		{
//		
//				Instantiate (explosionDelayed, other.transform.position, other.transform.rotation);
//		
//		
//		}
		public void  OnExplosion (GameObject other)
		{
		
				Instantiate (explosion, other.transform.position, other.transform.rotation);
		
				//dust
				for (int i = 0; i< globales.dustLevel; i++) {
						Instantiate (dust, transform.position, Quaternion.identity);
				}
		
		}
	
		// Update is called once per frame
		void Update ()
		{

				timer--;
				if (timer < 0) {
						Destroy (gameObject);
				}

				RaycastHit hit;

//				GameObject target = transform.gameObject.GetComponent<playerMovement> ().closest;
//				print ("ttt " + target);
//				if (target) {

//				Vector2 targetPos = transform.parent.transform.right * 8;
				Debug.DrawRay (transform.parent.transform.position, transform.parent.transform.right * length, Color.white);
				if (Physics.Raycast (transform.parent.transform.localPosition, transform.parent.transform.right * length, out hit)) {
		
		
						switch (hit.transform.gameObject.tag) {
						case "arana":

								OnExplosion (hit.transform.gameObject);
								

								Destroy (gameObject);
								globales.kills += 1;
		
								enemyController.GetComponent<enemyController> ().deleteSpider (hit.transform.gameObject);
								Instantiate (movingTextKills, hit.transform.gameObject.transform.position, hit.transform.gameObject.transform.rotation);
									
								break;
		
						case "snake":
								OnExplosion (hit.transform.gameObject);
								Destroy (gameObject);
//								enemyLife _enemyLife = hit.transform.gameObject.GetComponent<enemyLife> ();
									
										
								globales.kills += 1;
								enemyController.GetComponent<enemyController> ().deleteSnake (hit.transform.gameObject);
										
								Instantiate (paquete, hit.transform.gameObject.transform.position, hit.transform.gameObject.transform.rotation);
								Instantiate (movingTextKills, hit.transform.gameObject.transform.position, hit.transform.gameObject.transform.rotation);
										
								Destroy (gameObject);
								Destroy (hit.transform.gameObject);
//								hit.transform.gameObject.GetComponent<enemyLife> ().decreaseHP ();
//								enemyController.GetComponent<enemyController> ().createSnake ();
								
										
								break;
						}
//						}
				}
		}
}
