﻿using UnityEngine;
using System.Collections;

public class paqueteScr : MonoBehaviour
{

		public int wBlockBonus;
		public int laserBonus;
		public int TWayBonus;
		public int circularBonus;
		public int triBonus;
		public int moireBonus;
		public int bombaBonus;
		public int rayoBonus;

		public GameObject animPaquete;

		public int bulletType;

	
	
		void Start ()
		{
				bulletType = globales.getWeaponType ();

		
		}
	
		public void giveBullets ()
		{
//				switch (globales.currentWeapon) {
				switch ((globales.WEAPONS)bulletType) {
			
//				case globales.WEAPONS.NORMAL:
//						Destroy (gameObject);
//						break;
			
				case globales.WEAPONS.WBLOCK:
						globales.bullets [bulletType] += wBlockBonus;
						Destroy (gameObject);
						break;
			
				case globales.WEAPONS.LASER:
						globales.bullets [bulletType] += laserBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.TWAY:
						globales.bullets [bulletType] += TWayBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.CIRCULAR:
						globales.bullets [bulletType] += circularBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.BOMBA:
						globales.bullets [bulletType] += bombaBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.RAYO:
						globales.bullets [bulletType] += rayoBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.TRI:
						globales.bullets [bulletType] += triBonus;
						Destroy (gameObject);

						break;
				case globales.WEAPONS.MOIRE:
						globales.bullets [bulletType] += moireBonus;
						Destroy (gameObject);
			
						break;
			
				}
		}


		void OnTriggerEnter (Collider other)
		{


				if (other.gameObject.tag == "hole") {
						Destroy (gameObject);

				}

		}

		public void triggerAnim ()
		{
				Instantiate (animPaquete, transform.position, transform.rotation);
		}


}
