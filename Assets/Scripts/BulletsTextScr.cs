using UnityEngine;
using System.Collections;

public class BulletsTextScr : MonoBehaviour
{
		public  GUIStyle bulletsSt = new GUIStyle ();
		public  GUIStyle killsSt = new GUIStyle ();
		public GUIStyle readySt = new GUIStyle ();
		public GUIStyle keyHelpSt = new GUIStyle ();

		public Texture blackTexture;

//		public Texture backProgress;
//		public Texture foreProgress;

		public int left;
		public int top;


		public static Rect bulletsHUD;// = new Rect (5, 5, 60, 30);
		public static Rect killsHUD;// = new Rect (left, top, 60, 30);
		public static Rect stageHUD;// = new Rect (left, top, 60, 30);
		public static Rect enemyBarHUD;


		public static Vector2 bulletsHUDPos = new Vector2 (5, 5);
		public static Vector2 killsHUDPos = new Vector2 (5, 5);
		public static Vector2 stageHUDPos = new Vector2 (Screen.width / 2, 5);
		public static Vector2 enemyBarHUDPos = new Vector2 (Screen.width / 2, 10);

		public static int counter = 0;
		float alphaFadeValue = 1f;


	
		// Update is called once per frame
		void Update ()
		{
//				left = left;
//				top = top;
		}
	

		void OnGUI ()
		{

				//set pos
				var textDimensions = GUI.skin.label.CalcSize (new GUIContent ("KILLS 000"));
//				bulletsHUD = new Rect (5, 5, 60, 30);
				killsHUD = new Rect (Screen.width - textDimensions.x, 5 + top, 60, 30);
				stageHUD = new Rect (Screen.width / 2 - textDimensions.x, 5, 60, 30);
				enemyBarHUD = new Rect (Screen.width / 2, 10, 200, 30);

				//paint
//				if (gameControl.currentState == gameControl.State.INGAME) {

				for (int i = 0; i< globales.bullets.Length; i++) {
						if (globales.bullets [i] > 0) {
//								print ("TEXT DIMENSIONS " + textDimensions);
								bulletsHUD = new Rect (5, 25f * i, 60, 30);
								GUI.Label (bulletsHUD, ((globales.WEAPONS)i).ToString () + globales.bullets [i].ToString (), bulletsSt);
						}

				}







				GUI.Label (killsHUD, + globales.kills + "KILLS ", killsSt);
				GUI.Label (stageHUD, "AREA  " + (globales.currentMap + 1).ToString (), bulletsSt);
//				GUI.Label (stageHUD, "AQUI: PATH: " + Application.persistentDataPath, bulletsSt);

//				Debug.Log ("AQUI: PATH: " + Application.persistentDataPath);

				GUI.Label (new Rect (stageHUD.x, stageHUD.y + 35, stageHUD.width, stageHUD.height), "STAGE " + (globales.currentStage + 1).ToString (), bulletsSt);
//				GUI.Label (new Rect (stageHUD.x, stageHUD.y + 70, stageHUD.width, stageHUD.height), "TIME  " + (gameControl.stageTime).ToString ("F1"), bulletsSt);
				

				//READY DISPLAY
				if (gameControl.currentState == gameControl.State.INGAME && counter < 60) {

						counter++;
						GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 100, 100), "READY? ", readySt);
				} 

				if (gameControl.currentState != gameControl.State.INGAME) {
						counter = 0;
				}
				//CLEARED DISPLAY
				if (gameObject.GetComponent<gameControl> ().collectingTime) {
						GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 100, 100), "CLEARED!", readySt);

				}

				//SLOWMOTION DISPLAY
//				print (counter);
				if (gameControl.slowMotion && !gameObject.GetComponent<gameControl> ().collectingTime && counter >= 60 && gameControl.stageTime > 1.8f) {

						if (alphaFadeValue > 0.5f) {
								alphaFadeValue = alphaFadeValue - 0.1f;
						}
						GUI.color = new Color (0, 0, 0, alphaFadeValue);
						GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), blackTexture);
//						GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 100, 100), "READY??", readySt);
						GUI.Label (new Rect (Screen.width / 2, Screen.height - 20f, 100, 100), "DROP YOUR FINGER ON SCREEN TO SURVIVE", keyHelpSt);

				} 


//				GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 100, 100), ((gameObject.GetComponent<gameControl> ().gatheringTime) * -1).ToString ("F2"), bulletsSt);

	

				//						GUI.Label (enemyBarHUD, "ENEMIES " + enemyController.enemies.Count, bulletsSt);


//						GUI.DrawTexture (enemyBarHUD, backProgress);

//						int totalEnemies = gameControl.stages [gameControl.stageNumber].snakes + gameControl.stages [gameControl.stageNumber].spiders;
//						GUI.DrawTexture (new Rect (enemyBarHUD.x, enemyBarHUD.y, enemyBarHUD.size.x * enemyController.enemies.Count / totalEnemies, enemyBarHUD.size.y), foreProgress);
//			enemyController.enemies.Count
//				}

		}
}
