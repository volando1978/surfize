 using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InterludeScr : MonoBehaviour
{

		public static InterludeScr instance = null;

		public static InterludeScr Instance {
				get {
						return instance;
				}
		}

		public GUIStyle marqueeSt = new GUIStyle ();
		public GUIStyle marqueeBigSt = new GUIStyle ();
		public GUIStyle boxStyle = new GUIStyle ();

//		public List<string> congratsText = new List<string> ();

		public string maxText;
		public string killsText;
		public string stageTimeText;
		public string timeText;
		public string bulletsCollectedText;
	
		Rect messageRect;
		Rect boxRect;

//		public GameObject winText;


		// Use this for initialization
		public void Start ()
		{

				StartCoroutine ("run");
//				GameObject w = Instantiate (winText)as GameObject;
//				w.transform.parent = transform;

		}

		public IEnumerator run ()
		{
				boxRect = new Rect (0, Screen.height / 2 - 16f, 0, 0);
		
				maxText = globales.maxKills.ToString ();
				killsText = globales.kills.ToString ();
				stageTimeText = globales.lastKills.ToString ();
				bulletsCollectedText = globales.bulletsCollected.ToString ();
		
				if (globales.maxKills <= globales.kills) {
						globales.maxKills = globales.kills;
				} 
				yield return new WaitForSeconds (2f);

		}

//		public void removeMe ()
//		{
//				StartCoroutine ("runOut");
//		}
//
//		public IEnumerator runOut ()
//		{
//				if (boxRect != null) {
////			if (boxRect.height < 200f) {
////				boxRect.height += 160f;
////			}
//			
//						if (boxRect.width >= Screen.width) {
//								boxRect.x += 80f;
//						}
//
//						if (boxRect.x < Screen.width) {
//								boxRect.x += 30f;
//
//								yield return new WaitForSeconds (2f);
//						} else {
//
//								Destroy (gameObject);
//						}
//
//				} 
//		
//		}
	
	
		void OnGUI ()
		{
				// black box in background
				GUI.Box (boxRect, "", boxStyle);
		
				if (boxRect.height < 200f) {
						boxRect.height += 160f;
				}

				if (boxRect.width < Screen.width) {
						boxRect.width += 80f;
				}


				//texts stats
				killsText = "KILLS: " + globales.kills;
				maxText = "MAX: " + globales.maxKills;
				stageTimeText = "TIME: " + gameControl.stageTime.ToString ("F1");
				bulletsCollectedText = "SAMPLE EGGS: " + globales.bulletsCollected.ToString ();


				var dimensions = GUI.skin.label.CalcSize (new GUIContent (maxText));
				messageRect.width = dimensions.x;
				messageRect.height = dimensions.y;
				
				messageRect.x = Screen.width / 2;//0.5f;//200f;//Camera.main.ScreenToWorldPoint (Screen.width - Screen.width / 2);
				messageRect.y = Screen.height / 2;//0.5f;//20f;//Camera.main.ScreenToWorldPoint (Screen.height + Screen.height / 2);

				//LEFT SIDE
				messageRect.x = dimensions.x - 40;
		
				GUI.Label (messageRect, "STAGE: " + globales.currentStage, marqueeBigSt);

				//RIGHT SIDE
				messageRect.x = Screen.width / 2;//0.5f;//200f;//Camera.main.ScreenToWorldPoint (Screen.width - Screen.width / 2);


				GUI.Label (messageRect, killsText, marqueeSt);
				messageRect.y += dimensions.y + 10;

				GUI.Label (messageRect, stageTimeText, marqueeSt);

				messageRect.y += dimensions.y + 10;
		
				GUI.Label (messageRect, bulletsCollectedText, marqueeBigSt);


		}
	
		public void OnApplicationQuit ()
		{
				Destroy (gameObject);
		}
	
		public void DestroyInstance ()
		{
//				print ("menu instance destroyed");
	
				instance = null;
		}
}
