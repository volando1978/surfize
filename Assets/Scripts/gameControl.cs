﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Stage
{
		public int holes;
		public int spiders;
		public int snakes;
		public float time;
		public int bombs;
		public int rays;
		public int repeteas;

		public float intervalStage;
		public string stageIntro;
//		public float stageTime;
		public int bulletsCollected;
		public bool isStageCleared = false;

}

[System.Serializable]
public class Map
{

		public Stage[] stages;
		public bool isMapCleared;
	
}


public class gameControl : MonoBehaviour
{

		//constructor Instance
		private static gameControl instance;

		public static gameControl Instance {
				get {
						return instance;
				}
		}

		//--STATES
		public enum State
		{
				MENU = 0,
				INTERLUDE = 1,
				WEAPONROOM = 2,
				MAP = 3,
				INGAME = 4,
				NEWWEAPON = 5,
				GAMEOVER = 6
		}


		[SerializeField]
		public static State
				currentState;

		[SerializeField]
		State
				status;

	

		protected Vector2 midPos = Vector2.zero;

		public GameObject player;
		GameObject currentPlayer;

		public GameObject enemyController;
		public GameObject currentEnemyController;
		enemyController _enemyController;

		public GameObject menuObj;
		GameObject currentMenu;

		public GameObject interlude;
		GameObject currentInterlude;

		public GameObject weaponRoom;
		GameObject currentWeaponRoom;

		public GameObject mapObj;
		GameObject currentMapObj;

		public GameObject bombaObj;
		public GameObject dustObj;
		public GameObject explosionExpansivaObj;
		public GameObject paquete;

		public GameObject newWeaponRoom;
		public GameObject currentNewWeaponRoom;

		public GameObject gameOverObj;
		public GameObject currentGameOver;

		// -- BULLETS
		public GameObject bullet;
		public GameObject bulletThin;
		public GameObject bulletLaser;
		public GameObject raycastObj;
		public GameObject circController;
		public GameObject rayo;

		//--BOMBAS
		public GameObject bomba;
		public GameObject rayoBomba;

		//--PROXY
		public GameObject naveProxy;
		GameObject currentNaveProxy;

		//--motivations sprites
		public float gatheringTime;
		public static float stageTime;

		//--MAPS
	
		[SerializeField]
		protected Map[]
				maps;

		Map map;

		[SerializeField]
		public static int
				totalNumberStages;

		Stage stage;
		public static bool stageDeployed;

		[SerializeField]
		bool
				newWeaponFlag = false;

		public bool collectingTime = false;
		public static bool slowMotion = false;

	


		// -----------------------------------------------------------------------------
		// -----------------------------------------------------------------------------

		public void Start ()
		{
				currentState = State.MENU;
				initMenu ();
				setStage ();
				setWeapon ();

		}


		void Update ()
		{
//				print ("state: " + currentState);
				status = currentState;


				#if UNITY_EDITOR 
				if (InputHelper.space ()) {
						switch (currentState) {
						case State.MENU:
								currentState = State.MAP;
								toMap ();
								removeMenu ();
								break;
						case State.MAP:
//								print ("PASA POR MAPA");
								if (MapScr.isReady ()) {
										currentState = State.INGAME;
//										toWeaponRoom ();
										removeMap ();
										StartCoroutine ("startGame");

								}
								break;	
						case State.INTERLUDE:
								globales.setData ();
								if (newWeaponFlag) {
										removeInterlude ();
										toNewWeapon ();
										currentState = State.NEWWEAPON;
								} else {
										removeInterlude ();
										toMap ();
										currentState = State.MAP;
								}
								break;
//						case State.WEAPONROOM:
//								removeWeaponRoom ();
//								StartCoroutine ("startGame");
//								currentState = State.INGAME;
								break;	

						case State.NEWWEAPON:
//								print ("is newWEAPON READY?" + newWeaponRoomSrc.isReady ());
								if (newWeaponRoomSrc.isReady ()) {
										//														MapScr.setReady (false);
										removeNewWeapon ();
										toMap ();
										currentState = State.MAP;
								}
								break;
						case State.GAMEOVER:
			//						if (gameOverSrc.isReady ()) {
			
			
			
								break;
						case State.INGAME:
								break;
			
						}
				}
		
				#endif
		
				#if UNITY_IPHONE 
				for (int i = 0; i < Input.touchCount; ++i) {
						if (Input.GetTouch (i).phase.Equals (TouchPhase.Began)) {
								Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
								RaycastHit hit;
	
								//toca item
								if (Physics.Raycast (ray, out hit) && hit.collider.gameObject.layer != 5) {
										switch (currentState) {
										case State.MENU:
												currentState = State.MAP;
												toMap ();
												removeMenu ();
												break;
										case State.MAP:
												if (MapScr.isReady ()) {
														currentState = State.INGAME;
														//										toWeaponRoom ();
														removeMap ();
														StartCoroutine ("startGame");
							
												}
												break;	
										case State.INTERLUDE:
												globales.setData ();
												if (newWeaponFlag) {
														removeInterlude ();
														toNewWeapon ();
														currentState = State.NEWWEAPON;
												} else {
														removeInterlude ();
														toMap ();
														currentState = State.MAP;
												}
												break;
//										case State.WEAPONROOM:
//												removeWeaponRoom ();
//												StartCoroutine ("startGame");
//												currentState = State.INGAME;
												break;	
						
										case State.NEWWEAPON:
						//								print ("is newWEAPON READY?" + newWeaponRoomSrc.isReady ());
												if (newWeaponRoomSrc.isReady ()) {
														//														MapScr.setReady (false);
														removeNewWeapon ();
														toMap ();
														currentState = State.MAP;
												}
												break;
										case State.GAMEOVER:
						//						if (gameOverSrc.isReady ()) {
						
						
						
												break;
										case State.INGAME:
												break;
						
										}
								}	
						}
				}
				#endif
		
				//salto MAP
//				if (currentState == State.MAP && MapScr.isReady ()) {
//						currentState = State.WEAPONROOM;
//						toWeaponRoom ();
//						removeMap ();
//				}

				if (currentState == State.INGAME) {
						stageTime += Time.deltaTime;
						//sube las luces durante el juego
//						Color32 tempColor = (Color32)Camera.main.backgroundColor;
////						tempColor.a = (byte)(Camera.main.backgroundColor.a + stageTime * 10f);
//						tempColor.r = (byte)(Camera.main.backgroundColor.r + stageTime % 2f);
//						tempColor.g = (byte)(Camera.main.backgroundColor.g + stageTime % 2f);
//						tempColor.b = (byte)(Camera.main.backgroundColor.b + stageTime % 2f);
//						print (tempColor.a);
//
//						Camera.main.backgroundColor = tempColor;
				}

//				for (int input = 0; input < Input.touchCount; ++input) {
//						if (input <= 0 && gameControl.currentState
				if (Input.touchCount == 0 && gameControl.currentState == gameControl.State.INGAME && BulletsTextScr.counter >= 60 && collectingTime == false) {
						Time.timeScale = 0.2f;
						slowMotion = true;
				} else {
//						if (input >= 0) {

						slowMotion = false;
						Time.timeScale = 1f;
				}
		}
		void FixedUpdate ()
		{
				if (_enemyController) {
						_enemyController.moveEnemies ();
				}
				
				//JUEGO
				if (currentState == State.INGAME && _enemyController) {

						currentPlayer.GetComponent<playerMovement> ().move ();
						currentPlayer.GetComponent<playerMovement> ().rotateShipDirection (_enemyController);


						currentPlayer.GetComponent<disparo> ().dispara (_enemyController, bullet, bulletThin, circController, bulletLaser, raycastObj);

						if (isStageClear ()) {

								StartCoroutine ("stageClearTransition");
								StartCoroutine ("clearHoles");
								stageDeployed = false;
				
						}
			
				}
		
		}

		//------------STAGES--------------------
		//--------------------------------------
		public void setStage ()
		{
		
				//TODO: picked objects	
		
				//-- SET TOTAL NUMBER STAGES PER MAP
				if (totalNumberStages == 0) {
						totalNumberStages = maps [globales.currentMap].stages.Length;
//						print (" init totalNumberStages: " + totalNumberStages);
				}
		
				if (globales.totalNumberMaps == 0) {
						globales.totalNumberMaps = maps.Length;
//						print ("init totalNumberMaps: " + globales.totalNumberMaps);
				}
		
		
				//--  LOOP OF STAGES
				if (globales.currentStage >= totalNumberStages) {
//						maps [globales.currentMap].stages [globales.currentStage].isStageCleared = true;
						maps [globales.currentMap].isMapCleared = true;

						globales.currentMap = globales.currentMap + 1;
						stage = maps [globales.currentMap].stages [0];
						globales.currentStage = 0;
						totalNumberStages = maps [globales.currentMap].stages.Length;
						newWeaponFlag = true;
			
				} else {

						stage = maps [globales.currentMap].stages [globales.currentStage];

			
				}
				//STORE STAGE STATS
//				print ("currentStage: " + globales.currentStage);
		
				maps [globales.currentMap].stages [globales.currentStage].time = stageTime;
		
		}

		public bool isStageClear ()
		{
				bool isCleared = false;
				if (stageDeployed && GameObject.FindGameObjectsWithTag ("arana").Length < 1 && GameObject.FindGameObjectsWithTag ("snake").Length < 1) {
						isCleared = true;
				} 
		
				return isCleared;
		
		}

	
	
	
		public IEnumerator stageClearTransition ()
		{
				collectingTime = true;
				print ("stage is cleared");
				//TODO: Stage is clear
		
				yield return new WaitForSeconds (gatheringTime);
				maps [globales.currentMap].stages [globales.currentStage].isStageCleared = true;
				maps [globales.currentMap].stages [globales.currentStage].bulletsCollected = globales.bulletsCollected;
				globales.currentStage++;
		
				setStage ();

				currentState = State.INTERLUDE;
				toInterlude ();

				yield return null;
				collectingTime = false;
		
		
		}
	
	
	
	
		//-----------WEAPONS-----------
		//-----------------------------
	
	
		public void setWeapon ()
		{
		
				if (globales.unlockedWeapons == null) {
						globales.unlockedWeapons = globales.WEAPONS.WBLOCK;
			
				}

		}

		//---------GAME-------------
		//--------------------------
	

		public  IEnumerator startGame ()
		{
				globales.bulletsCollected = 0;
				stageTime = 0;
				bool start = false;
				stageDeployed = false;
				globales.setGrey ();
				naveProxyController.gizmosReady = false;
				

		
				while (!start) {
			
						currentState = State.INGAME;
						clearEnemies ();
						globales.clearMenu ();
//						globales.resetStats ();
						//borra logo
						GetComponent<SpriteRenderer> ().enabled = false;
						//create player
						currentPlayer = (GameObject)Instantiate (player, midPos, Quaternion.identity);
			
			
						//create enemy controller
						if (!currentEnemyController) {
//								print ("recrea el currentEnemyController");
								currentEnemyController = (GameObject)Instantiate (enemyController, midPos, Quaternion.identity);
								_enemyController = currentEnemyController.GetComponent<enemyController> ();
						}
			
						start = true;
			
				}
		
		
				while (!stageDeployed) {
			
						for (int i = 0; i < stage.holes; i++) {
				
								_enemyController.createHoles ();
//								_enemyController.initHoles ();

				
						}
						yield return new WaitForSeconds (stage.intervalStage + 2);
						//create enemy for stage
						for (int i = 0; i < stage.spiders; i++) {
								if (!stageDeployed) {

										_enemyController.createSpider ();
								}
								//						print ("stage . spiders: " + stage.spiders + " ::: " + i);
								yield return new WaitForSeconds (Random.Range (0, stage.intervalStage));
				                                 
						}
			
						for (int i = 0; i < stage.repeteas; i++) {
								if (!stageDeployed) {

										Instantiate (naveProxy, globales.getRandomPos (), Quaternion.identity);
								}
								yield return new WaitForSeconds (stage.intervalStage);
				
						}
						for (int i = 0; i < stage.snakes; i++) {
								if (!stageDeployed) {

										_enemyController.createSnake ();
										//						print ("stage . snakes: " + stage.snakes + " ::: " + i);
								}
								yield return new WaitForSeconds (Random.Range (0, stage.intervalStage));
				
						}

						for (int i = 0; i < stage.bombs; i++) {
								if (!stageDeployed) {

										Instantiate (bomba, globales.getRandomPos (), Quaternion.identity);
								}
								yield return new WaitForSeconds (stage.intervalStage);
				
						}
						for (int i = 0; i < stage.rays; i++) {
								if (!stageDeployed) {

										Instantiate (rayoBomba, globales.getRandomPos (), Quaternion.identity);
								}
								yield return new WaitForSeconds (stage.intervalStage);
				
						}
			
						stageDeployed = true;
//						print ("stageDeployed: " + stageDeployed);
				}
		}
	
	
	
		public void finishGame ()
		{
				clearPlayer ();

//				clearPowerUps ();
//				clearEnemies ();

				Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shake", 16f);
		
				//display logo
				GetComponent<SpriteRenderer> ().enabled = false;
				globales.lastKills = globales.kills;

		
				toGameOverRoom ();
		}


		//------------CLEARERS-------------
		//---------------------------------




		void clearEnemies ()
		{
				if (_enemyController) {
						_enemyController.deleteHoles ();
//						_enemyController.initHoles ();
						//clear enemies
						_enemyController.clearEnemies ();
				}

		}

	
		public IEnumerator clearHoles ()
		{
		
				GameObject[] gos = GameObject.FindGameObjectsWithTag ("hole");
		
				foreach (GameObject go in gos) {
//						if (go.transform.position.y < 500) {
						GameObject e = Instantiate (explosionExpansivaObj, go.transform.position, go.transform.rotation)as GameObject;

						for (int i = 0; i< globales.dustLevel; i++) {
//								GameObject g = Instantiate (dustObj, go.transform.position, go.transform.rotation)as GameObject;
//								g.GetComponent<SpriteRenderer> ().color = Color.white;
//								globales.lanzaEnDirecciones (g);

								GameObject p = Instantiate (paquete, go.transform.position, go.transform.rotation)as GameObject;
								globales.lanzaEnDirecciones (p);

						}
			
						go.transform.Translate (Vector3.down * Time.deltaTime * 3000f);
				
						yield return new WaitForSeconds (0.1f);
				
//						}
				}
				yield return null;
		}
	
	
	
	
	
		public void clearPlayer ()
		{
		
				Destroy (currentPlayer);
		}


		public void clearPowerUps ()
		{

//				GameObject[] gosProxy = GameObject.FindGameObjectsWithTag ("NaveProxy");
				GameObject[] gosRays = GameObject.FindGameObjectsWithTag ("rayoBomba");
				GameObject[] gosBomb = GameObject.FindGameObjectsWithTag ("bomba");
				GameObject[] gosPaquete = GameObject.FindGameObjectsWithTag ("paquete");

		
//				foreach (GameObject go in gosProxy) {
//						Destroy (go);
//				}
				foreach (GameObject go in gosRays) {
						Destroy (go);
				}
				foreach (GameObject go in gosBomb) {
						Destroy (go);
				}
				foreach (GameObject go in gosPaquete) {
						Destroy (go);
				}
		
		
		
		}


		//-------------POW UPS-------------
		//---------------------------------

		GameObject createNaveProxy ()
		{
		
				currentNaveProxy = Instantiate (naveProxy, globales.getRandomPos (), Quaternion.identity) as GameObject;
				return currentNaveProxy;
		}
	
		// ---FUNCIONES PARA ESTADOS-------
		//---------------------------------

	

		// INSTANTIATE STATES

	
		public void initMenu ()
		{
				currentMenu = (GameObject)Instantiate (menuObj, midPos, Quaternion.identity);
//				globales.resetStats ();
		
		}
	
		public void removeMenu ()
		{
				if (currentMenu) {
						Destroy (currentMenu);
				}
		}

		public void toInterlude ()
		{
				clearPlayer ();
				clearPowerUps ();
				currentInterlude = (GameObject)Instantiate (interlude, midPos, Quaternion.identity);

		}

		public void removeInterlude ()
		{
				if (currentInterlude) {
						Destroy (currentInterlude);
				}
		}

		public void toWeaponRoom ()
		{
				if (!currentWeaponRoom) {
						currentWeaponRoom = (GameObject)Instantiate (weaponRoom, midPos, Quaternion.identity);
				}
		
		}

		public void removeWeaponRoom ()
		{
				if (currentWeaponRoom) {
						Destroy (currentWeaponRoom);
				}
		}


	
		public void toMap ()
		{
//				globales.cameraColor ();
				globales.setRandomColor ();

				if (!currentMapObj) {
						currentMapObj = (GameObject)Instantiate (mapObj, midPos, Quaternion.identity);
						MapScr.setReady (false);
						currentMapObj.SetActive (true);
				} else {

				}
		
		}
	
		public void removeMap ()
		{
				if (currentMapObj) {
						MapScr.setReady (true);
						currentMapObj.SetActive (false);
						Destroy (currentMapObj);
				}
		}

		public void toNewWeapon ()
		{
				if (!currentNewWeaponRoom) {
						currentNewWeaponRoom = (GameObject)Instantiate (newWeaponRoom, midPos, Quaternion.identity);
						newWeaponRoomSrc.setReady (false);
						newWeaponFlag = false;

				}
		
		}
	
		public void removeNewWeapon ()
		{
				if (currentNewWeaponRoom) {
						Destroy (currentNewWeaponRoom);
				}
		}

		public void toGameOverRoom ()
		{
				if (!currentGameOver) {
						currentGameOver = (GameObject)Instantiate (gameOverObj, midPos, Quaternion.identity);
						//				globales.resetStats ();
				}
		
		}
	
		public void removeGameOverRoom ()
		{
				if (currentGameOver) {
						Destroy (currentGameOver);
				}
		}
	
	
		public void OnApplicationQuit ()
		{
				Destroy (gameObject);
		}

}
