﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour
{

		public float speed;
		public float angle;
		public float step;


		Vector3 p;

		public GameObject enemyController;
		public GameObject paquete;
		public GameObject movingText;
		public GameObject movingTextKills;



		public Sprite thinBullet;
		public Sprite fatBullet;
//		public Sprite laserBullet;
		public Sprite triBullet;
		public Sprite circularBullet;

		SpriteRenderer _spriterenderer;
		public GameObject explosion;
		public GameObject dust;


	
	

		void  OnExplosion ()
		{
				Instantiate (explosion, transform.position, Quaternion.identity);

				//dust
				for (int i = 0; i< globales.dustLevel; i++) {
						Instantiate (dust, transform.position, Quaternion.identity);
				}
		
		}

		
		void Start ()
		{
				_spriterenderer = GetComponentInChildren<SpriteRenderer> ();
				setBulletType ();
		}
	
		void OnTriggerEnter (Collider other)
		{	
				if (other.tag == "arana") {
						Destroy (gameObject);
//						print (enemyController.GetComponent<enemyController> ().getNumberEnemies ());

						OnExplosion ();
						
						globales.kills += 1;
						enemyController.GetComponent<enemyController> ().deleteSpider (other.gameObject);

						Instantiate (paquete, transform.position, transform.rotation);
						Instantiate (movingTextKills, transform.position, transform.rotation);

						Destroy (gameObject);
				}

				if (other.tag == "snake") {
						Destroy (gameObject);
//						print (enemyController.GetComponent<enemyController> ().getNumberEnemies ());

						OnExplosion ();
						
						enemyLife _enemyLife = other.GetComponent<enemyLife> ();

						if (_enemyLife.hp < 0) {
								enemyController.GetComponent<enemyController> ().deleteSnake (other.gameObject);

								globales.kills += 1;
								Instantiate (paquete, transform.position, transform.rotation);
								Instantiate (movingTextKills, transform.position, transform.rotation);

								Destroy (gameObject);
						} else {
								other.GetComponent<enemyLife> ().decreaseHP ();
						}
				}
		}

	



		public void setBulletType ()
		{

				if (globales.bullets [(int)globales.currentWeapon] < 1) {

						_spriterenderer.sprite = thinBullet;

				} else {
						switch (globales.currentWeapon) {

						case globales.WEAPONS.WBLOCK:
								_spriterenderer.sprite = fatBullet;
								break;
			
//						case globales.WEAPONS.LASER:
//								_spriterenderer.sprite = laserBullet;

//								break;
						case globales.WEAPONS.TWAY:
								_spriterenderer.sprite = triBullet;

								break;
						case globales.WEAPONS.CIRCULAR:
								_spriterenderer.sprite = circularBullet;

			
								break;
						case globales.WEAPONS.BOMBA:
			
								break;
						case globales.WEAPONS.RAYO:
			
								break;
						case globales.WEAPONS.TRI:
								break;
						default:
								_spriterenderer.sprite = thinBullet;
								break;
			
						}
				}


		}

	
//		void OnDrawGizmos ()
//		{
//				Color color;
//				color = Color.white;
//				// local up
//				DrawHelperAtCenter (this.transform.up, color, 1f);
//		
////				color.g -= 0.5f;
////				// global up
////				DrawHelperAtCenter (Vector3.up, color, 1f);
//		
//				color = Color.blue;
//				// local forward
//				DrawHelperAtCenter (this.transform.forward, color, 1f);
//		
////				color.b -= 0.5f;
////				// global forward
////				DrawHelperAtCenter (Vector3.forward, color, 1f);
//		
//				color = Color.red;
//				// local right
//				DrawHelperAtCenter (this.transform.right, color, 1f);
//		
////				color.r -= 0.5f;
////				// global right
////				DrawHelperAtCenter (Vector3.right, color, 1f);
//		}
//	
//		private void DrawHelperAtCenter (Vector3 direction, Color color, float scale)
//		{
//				Gizmos.color = color;
//				Vector3 destination = transform.position + direction * scale;
//				Gizmos.DrawLine (transform.position, destination);
//		}

}
