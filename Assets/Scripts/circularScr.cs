﻿using UnityEngine;
using System.Collections;

public class circularScr : MonoBehaviour
{

//		public float speed;
//		public float angle;
//		public float step;
//	
	

		public GameObject enemyController;
		public GameObject paquete;
		public GameObject movingText;
		public GameObject movingTextKills;
		public GameObject explosion;

		public GameObject circularControler;
		GameObject center;
		public float radius;

		bool release = false;
		public GameObject explosionDelayed;
		public GameObject dust;

	
	
	
	
		void  OnExplosionDelayed ()
		{
		
				Instantiate (explosionDelayed, transform.position, Quaternion.identity);
		
		
		}
		// Use this for initialization
		void Start ()
		{
//				if (center == null) {
//							
//						center = transform.parent.transform.gameObject;//GameObject.FindGameObjectWithTag ("Player");
//						transform.position = (center.transform.position);// + center.transform.position;
//				} else {
//						Destroy (gameObject);
//				}

		}
	
		// Update is called once per frame
		void Update ()
		{

				if (globales.bullets [(int)globales.currentWeapon] <= 0) {
				} else {
						release = true;
//						GameObject g = GameObject.FindGameObjectWithTag ("circCont");
//						Destroy (g);
//						Destroy (transform.parent.gameObject);
				}


				if (release) {
						GameObject target = GameObject.FindGameObjectWithTag ("enemyController").GetComponent<enemyController> ().getClosest (transform.position);
//						print ("target: " + target);
						if (target) {
								transform.position = Vector3.MoveTowards (transform.position, target.transform.position, Random.Range (14f, 22f) * Time.deltaTime);		
								Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 3f);
						} else {
								Destroy (gameObject);
						}

				} else {
						transform.RotateAround (transform.parent.transform.position, Vector3.forward, 2f);
//						transform.RotateAround (GameObject.FindGameObjectWithTag.transform.position, Vector3.forward, 2f);


				}
		}

		public void  OnExplosion ()
		{
		
				Instantiate (explosion, transform.position, Quaternion.identity);
		
				//dust
				for (int i = 0; i< globales.dustLevel; i++) {
						Instantiate (dust, transform.position, Quaternion.identity);
				}
		
		}
	
	
		void OnTriggerEnter (Collider other)
		{	
				if (other.tag == "arana") {
						Destroy (gameObject);
//						print (enemyController.GetComponent<enemyController> ().getNumberEnemies ());
						if (enemyController.GetComponent<enemyController> ().getNumberEnemies () < 2) {
								OnExplosionDelayed ();
						} else {
								OnExplosion ();
						}
						globales.kills += 1;
						enemyController.GetComponent<enemyController> ().deleteSpider (other.gameObject);
						//TODO: coordinar con una clase para las oleadas
			
//						enemyController.GetComponent<enemyController> ().createSpider ();
						Instantiate (paquete, transform.position, transform.rotation);
			
						Instantiate (movingTextKills, transform.position, transform.rotation);
			
						Destroy (gameObject);
				}
		
				if (other.tag == "snake") {
//						print (enemyController.GetComponent<enemyController> ().getNumberEnemies ());
						if (enemyController.GetComponent<enemyController> ().getNumberEnemies () < 2) {
								OnExplosionDelayed ();
						} else {
								OnExplosion ();
						}
						Destroy (gameObject);
						enemyLife _enemyLife = other.GetComponent<enemyLife> ();
			
						if (_enemyLife.hp < 0) {
				
								globales.kills += 1;
								enemyController.GetComponent<enemyController> ().deleteSnake (other.gameObject);
								//TODO: coordinar con una clase para las oleadas
//								enemyController.GetComponent<enemyController> ().createSnake ();
								Instantiate (paquete, transform.position, transform.rotation);
				
								Instantiate (movingTextKills, transform.position, transform.rotation);
				
								Destroy (gameObject);
						} else {
								other.GetComponent<enemyLife> ().decreaseHP ();
						}
				}
		}

}

