﻿using UnityEngine;
using System.Collections;

public class enemyMov : MonoBehaviour
{

		[SerializeField]
		float
				speed;

		Vector2
				target;
		Vector3 p;

		private Quaternion _lookRotation;
		private Vector2 _direction;

		[SerializeField]
		int
				t = 200;
		[SerializeField]
		int
				op = -1;

		private Vector2 randomPos;

		void start ()
		{
				randomPos = globales.getRandomPos ();
				speed = Random.Range (1f, 3.5f);
		}


		void Update ()
		{
		
				if (t > 0) {
						t = t - 1;
				} else {
						
						op = (int)Random.Range (-1, 4);
//						print ("op " + op + " t " + t);
						t = 20;
			
				}

				if ((Vector2)transform.position == target) {
						randomPos = globales.getRandomPos ();
//						print (randomPos);
				}
		
		
		}


		public void move ()
		{
				GameObject player = GameObject.FindGameObjectWithTag ("Player");
				if (player) {

						if (op != 0 || op != 1) {
								target = player.transform.position;
						} 
				} else {
						target = globales.getRandomPos ();
				}

				if (op == 0) {
						target = globales.getRandomPos ();
				}

				if (op == 1) {
						GameObject g = (GameObject)enemyController.posHoles [Random.Range (0, enemyController.posHoles.Count - 1)];
						target = g.transform.position;
				}
		
				float step = speed * Time.deltaTime;
				float angle = calculateAngle (target);
				p = new Vector3 (transform.position.x, transform.position.y, Mathf.Rad2Deg * angle);
//				print ("angle: " + p);
//				transform.position = Vector2.MoveTowards (transform.position, target, step);	//move_towards_direction
				transform.Translate (Vector2.right * step);
//				transform.rotation = Quaternion.Euler (p);
				transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler (p), step);
				Vector3 lockZ = new Vector3 (transform.position.x, transform.position.y, 0);
				transform.position = lockZ;

//				float angle = Mathf.Atan2 (target.x - transform.position.x, target.y - transform.position.y) * Mathf.Rad2Deg;
//				transform.Rotate (transform.position.x, transform.position.y, angle * 10f * Time.deltaTime);

		}

		float calculateAngle (Vector2 target)
		{
				float ay = transform.position.y;
				float ax = transform.position.x;
				float bx = target.x;
				float by = target.y;
		
				float angle = Mathf.Atan2 (by - ay, bx - ax);
		
				return angle;
		
		}
}
